from collections import Counter
from nltk.metrics import ConfusionMatrix
import glob

ref = []
labels = set("X")
for path in glob.glob('p36_annotatie_wessel/*/en.tok.off.pos'):
    text = open(path)
    for line in text:
        if len(line.split()) == 7:
            ref.append(line.split()[5])
            labels.add(line.split()[5])
        else:
            ref.append("X")

tagged = []
for path in glob.glob('Iris/*'):
    text = open(path)
    for line in text:
        if len(line.split()) == 7:
            tagged.append(line.split()[5])
            labels.add(line.split()[5])
        else:
            tagged.append("X")

cm = ConfusionMatrix(ref, tagged)

print(cm)

true_positives = Counter()
false_negatives = Counter()
false_positives = Counter()

for i in labels:
    for j in labels:
        if i == j:
            true_positives[i] += cm[i,j]
        else:
            false_negatives[i] += cm[i,j]
            false_positives[j] += cm[i,j]

print("TP:", sum(true_positives.values()), true_positives)
print("FN:", sum(false_negatives.values()), false_negatives)
print("FP:", sum(false_positives.values()), false_positives)
print()

for i in sorted(labels):
    if true_positives[i] == 0:
        fscore = 0
    else:
        precision = true_positives[i] / float(true_positives[i]+false_positives[i])
        recall = true_positives[i] / float(true_positives[i]+false_negatives[i])
        fscore = 2 * (precision * recall) / float(precision + recall)
    print(i, fscore)
