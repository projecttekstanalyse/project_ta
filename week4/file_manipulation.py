# file_manipulation.py
# Authors: Franka Korpel, Wessel Poelman and Iris Meijer
# Date: 14-05-2019

import nltk
import glob

def pos_tagger(text, path):
    ''' create a new file with an added column with the pos tag in it '''
    output_f = open(path + '.pos', 'w')
    for line in text:
        pos_tag = nltk.pos_tag(nltk.word_tokenize(line.split()[3]))
        output_f.write(line.rstrip() + ' ' + pos_tag[0][1] + '\n')


def main():

    # Read the files and add the pos tags
    for path in glob.glob('p36/*/en.tok.off'):
        text = open(path)
        pos_tags = pos_tagger(text, path)

if __name__ == "__main__":
    main()
