# File name:    main.py
# Authors:      Wessel Poelman, Iris Meijer, Franka Korpel
# Date:         17-06-2019
#
# Description:  This is the main file for the entire wikification process.
#               It calls the different steps that are in the other files.


import glob
import nltk
from nltk.corpus import wordnet as wn
from ner_tagger import *
from synsets import *
from wikification import *
from measures import *
from tag_selection import *


def main():

    # Define the synset filter for interesting entities.
    # This can be changed to include more or less entities.
    # The filter is defined once here for performance reasons
    # (otherwise it would be recreated each loop)
    synset_filter = {
        'ENT': wn.synsets("entertainment", pos='n'),
        'ANI': wn.synsets("animal", pos='n'),
        'SPO': wn.synsets("sport", pos='n'),
    }

    # Define the synset exclusions from the interesting entities.
    # This can be changed to include more or less exclusions.
    # The exclusions have to be correct wordnet synsets.
    # Example: 'soldier' would be labeled ANI (animal) because
    # it's synsets included 'ant soldier'.
    synset_exclusions = {wn.synset('person.n.01'), wn.synset('group.n.01')}

    # Read all files to wikify & tag
    for input_file, output_file in zip(sorted(glob.glob('files/original/*/*/en.tok.off.pos')), sorted(glob.glob('files/tagged/*/*/en.tok.off.pos.ent'))):
        print(input_file, output_file)
        word_list_file, tagged_list_syns, tagged_list_ner, wikified_list = [], [], [], []
        # Read in the file and take the text
        with open(input_file) as text:

            for line in text:
                # First send off the text to filter through synsets
                # this is mostly for the 'Animals' (ANI), 'Sport' (SPO) and 
                # 'Entertainment' (ENT) categories. The filter can be changed!
                tagged_list_syns.append(synset_filter_procedures(line, synset_filter, synset_exclusions))
                # Create a wordlist of all the words in the file for the NER tagger
                word_list_file.append(line.split()[3])

            # Send all the words to the NER tagger
            tagged_list_ner = ner_tag(word_list_file)

        # Create a list with *possible* tags from both the Synset and NER taggers.
        # In this procedure, the NER tags have a higher priority and are chosen if
        # a word has both a synset AND a NER tag.
        combined_tag_list = combine_syns_ner(tagged_list_syns, tagged_list_ner)

        # Wikify the combined list, this procedure includes filtering the tags,
        # creating search queries, looking up the wikipedia links and writing to
        # new en.tok.off.pos.ent files.
        wikified_list = wikification(combined_tag_list, word_list_file)

        # Write the results to the file
        write_to_file(word_list_file, wikified_list, output_file, input_file)
    
    # Init variables for measurements
    labels = set("X")
    gold_std, tagged_ent, accuracy = [], [], [0, 0]

    # Create gold standard for measurements
    for path in sorted(glob.glob('files/original/*/*/en.tok.off.pos.ent')):
        with open(path) as gold_std_path:
            create_gold_std(gold_std_path, gold_std, labels)
    
    # Create tagged entities for measurement
    for path in sorted(glob.glob('files/tagged/*/*/en.tok.off.pos.ent')):
        with open(path) as tagged_ent_path:
            create_tagged(tagged_ent_path, tagged_ent, labels)

    # Generate the measurements: Confusion Matrix, precision, recall
    calculate_measurements(gold_std, tagged_ent, labels)

    for gold_path, tagged_path in zip(sorted(glob.glob('files/original/*/*/en.tok.off.pos.ent')), sorted(glob.glob('files/tagged/*/*/en.tok.off.pos.ent'))):
        with open(gold_path) as gold_text, open(tagged_path) as tagged_text:
            for gold_line, tagged_line in zip(gold_text, tagged_text):
                gold_split = gold_line.split()
                tagged_split = tagged_line.split()
                if len(gold_split) == 7:
                    accuracy[0] += 1
                    if len(tagged_split) == 7:
                        if tagged_split[6] == gold_split[6]:
                            accuracy[1] += 1
    print("Accuracy (tagged_correctly / total):", accuracy[1], " / ", accuracy[0], " = ", round((accuracy[1] / accuracy[0]) * 100), "%")




if __name__ == "__main__":
    main()