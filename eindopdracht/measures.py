# File name: measures.py
# Authors: Wessel Poelman, Iris Meijer, Franka Korpel
# Date:
#
# Description:  This file will read in the gold standard and the 
#               tagged data and calculates the precicion, recall 
#               and accuracy of that tagged data. It also creates a 
#               confusion matrix. 


from collections import Counter
from nltk.metrics import ConfusionMatrix


def create_gold_std(gold_std_path, gold_std, labels):
    ''' Read the gold standard data set '''
    for line in gold_std_path:
        if len(line.split()) == 7:
            gold_std.append(line.split()[5])
            labels.add(line.split()[5])
        else:
            gold_std.append("X")


def create_tagged(tagged_ent_path, tagged_ent, labels):
    ''' Read the tagged data set '''
    for line in tagged_ent_path:
        if len(line.split()) == 7:
            tagged_ent.append(line.split()[5])
            labels.add(line.split()[5])
        else:
            tagged_ent.append("X")


def calculate_measurements(gold_std, tagged_ent, labels):
    ''' Calculates the performance of the system by comparing the
    tagged data with the gold standard. '''

    # Create a confusion matrix
    cm = ConfusionMatrix(tagged_ent, gold_std)
    print(cm)

    # Initialize the counters for scoring the performance
    true_positives = Counter()
    false_negatives = Counter()
    false_positives = Counter()

    # Caclulate the performance of the system
    for i in labels:
        for j in labels:
            if i == j:
                true_positives[i] += cm[i,j]
            else:
                false_negatives[i] += cm[i,j]
                false_positives[j] += cm[i,j]

    # Print the performance of the system
    print("TP:", sum(true_positives.values()), true_positives)
    print("FN:", sum(false_negatives.values()), false_negatives)
    print("FP:", sum(false_positives.values()), false_positives)
    print()

    # Caculate the precision, recall and fscore
    for i in sorted(labels):
        if true_positives[i] == 0:
            fscore = 0
        else:
            precision = true_positives[i] / float(true_positives[i]+false_positives[i])
            recall = true_positives[i] / float(true_positives[i]+false_negatives[i])
            fscore = 2 * (precision * recall) / float(precision + recall)
            print(i, "PRECISION: {0} \n RECALL: {1}".format(precision, recall))
        print(i, "F-SCORE: {0}".format(fscore))