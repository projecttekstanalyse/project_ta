# File name: synsets.py
# Authors: Wessel Poelman, Iris Meijer, Franka Korpel
# Date: 16-06-2019
#
# Description:  This file/function takes the en.tok.off.pos files as input
#               and filters the words through a 'discriminating synset filter'
#               to see if the word is an 'interesting entity'. It returns a new
#               new file with the possible interesting entities for further processing.


from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet as wn

lemmatizer = WordNetLemmatizer()


def hypernymOF(synset1, synset2):
    """Checks if the first synset is a hypernym
    of the second synset."""
    if synset1 == synset2:
        return True
    for hypernym in synset1.hypernyms():
        if synset2 == hypernym:
            return True
        if hypernymOF(hypernym, synset2):
            return True
    return False


def synset_filter_procedures(line, synset_filter, synset_exclusions):
    """
    Takes a word with a pos tag as input and checks if
    the word is a noun (NN) or plural noun (NNS). If that is
    the case, the plural nouns are converted to their lemma (NNS -> NN).
    The second input it the synset filter for finding 'interesting entities'
    and the possible tag for that entity. If the word is a *possible*
    interesting entity, print it with the *possible* tag.
    """
    
    # Initialize the word and pos tag
    word_original_form = line.split()[3]
    word_and_pos_tag = line.split()[3:5]
    word_and_tag = [word_original_form, 'O']
    hypernyms = lambda s: s.hypernyms()

    # Filter the words to only select normal nouns and the first letter is NOT uppercase
    if (word_and_pos_tag[1] in ('NN', 'NNS', 'NNP')) and (not word_original_form[0].isupper()):
        # Create lemma from plural nouns
        if word_and_pos_tag[1] == 'NNS':
            word_and_pos_tag[0] = lemmatizer.lemmatize(word_and_pos_tag[0], wn.NOUN)

        # Create a tuple with the noun and its synsets
        word_and_synsets = (word_and_pos_tag[0], wn.synsets(word_and_pos_tag[0], pos='n'))

        # Check if the synsets of the noun are possibly interesting, based on the filter
        if word_and_synsets[1]:
            for synset in word_and_synsets[1]:
                # Check if the hypernyms are part of the exclusions, if so skip this word
                if set(synset.closure(hypernyms)).intersection(synset_exclusions):
                    break
                for entity_tag in synset_filter.keys():
                    if hypernymOF(synset, synset_filter[entity_tag][0]):
                        word_and_tag[1] = entity_tag

    return word_and_tag
