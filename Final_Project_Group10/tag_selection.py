# File name: tag_selection.py
# Authors: Wessel Poelman, Iris Meijer, Franka Korpel
# Date: 16-06-2019
#
# Description:  Creates a list of *possible* tags from both the 
#               ner tagged words and the synset tagged words.
#               NER has priority and will be picked if a word has
#               two tags!

def combine_syns_ner(tagged_list_syns, tagged_list_ner):
    """Creates a list of *possible* tags from both the
        ner tagged words and the synset tagged words.
        NER has priority and will be picked if a word has
        two tags!"""

    combined_list = []

    for ner_tag, syns_tag in zip(tagged_list_ner, tagged_list_syns):
        tag_to_add = ner_tag
        if syns_tag[1] != 'O':
            tag_to_add = syns_tag
        if ner_tag[1] != 'O' and syns_tag != 'O':
            tag_to_add = ner_tag

        combined_list.append(tag_to_add)
    return combined_list
