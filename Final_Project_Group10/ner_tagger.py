# File name: ner_tagger.py
# Authors: Wessel Poelman, Iris Meijer, Franka Korpel
# Date: 16-6-2019
#
# Description: This program will provide entities with the tags: PER/COU/ORG using the Stanford NerTagger
#              It's input is the wordlist from a textfile, it returns a list with the words and the tags.
from nltk.parse import CoreNLPParser


def ner_tag(word_list):
    ner_tagger = CoreNLPParser(url='http://localhost:9000', tagtype='ner')
    ner = ner_tagger.tag(word_list)
    ner_list = []
    for word, tag in ner:
        if tag == 'PERSON':
            tag = 'PER'
        elif tag == 'LOCATION':
            tag = 'COU'
        elif tag == 'ORGANIZATION':
            tag = 'ORG'
        ner_list.append([word,tag])

    return ner_list
