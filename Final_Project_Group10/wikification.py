# File name: wikification.py
# Authors: Wessel Poelman, Iris Meijer, Franka Korpel
# Date:
#
# Description:  In this file the main wikification process happens,
#               including: making the search queries, applying the correct
#               tags, finding the wikipedia links, disambiguating the results
#               and writing to the file.

import wikipedia
import nltk
from nltk.corpus import wordnet
from nltk.wsd import lesk
from synsets import *
import glob

def check_multiple_search_tags(item_1, item_2):
    """Check if the tags of the items are the same."""
    return item_1[1] == item_2[1]


def create_search_query_list(tagged_list):
    """Create a list with a search query string, the current tag and
    the index of the item in the main file (tagged list). The search query
    can be n words long and gets longer if the tag of the next item in the 
    tagged word list is the same. This way 'New' 'York' would become 'New York'
    and so on."""

    prev_search_query, search_query_list, search_query = [], [], ""

    for index, item in enumerate(tagged_list):

        # First check if the item is the last item of the list
        # ( prevent index out of range error)
        if tagged_list[index] == tagged_list[-1]:
            if item[1] != 'O' and item[0] not in prev_search_query:
                search_query = item[0]

        # Check if the tag is not 'O' and not in the previous search query
        elif item[1] != 'O' and item[0] not in prev_search_query:
            search_query = item[0]
            test_item = tagged_list[index + 1]

            # Check if there the next word has the same tag
            if check_multiple_search_tags(item, test_item):

                # If that is the case, loop through the remaining list until 
                # it finds a differnt tag or an 'O'
                for new_item in tagged_list[index + 1:]:
                    if new_item[1] == 'O':
                        break
                    elif check_multiple_search_tags(new_item, test_item) and new_item[0] not in prev_search_query:
                        search_query += " " + new_item[0]
                    test_item = new_item

        # Add the search query to the list with the tag and the index from the main list (tagged_words) 
        if search_query.split() != prev_search_query:
            search_query_list.append([search_query, item[1], index])
        prev_search_query = search_query.split()

    return search_query_list


def lesk_filter(tagged_list, word_list_file):
    """This function specifies the tags COU (to COU/CIT/NAT) and ORG (to ORG/ENT).
    It does so by looking at the synsets from the inputwords using the lesk-algortihm.
    When the definition of the chosen synset is in a filter, a predefined set of words for the entities,
    this function asigns that *possibly* correct entity."""

    # These filters are 'hand-made' by looking at synsets of multiple countries/cities/organisations
    city_filter = ['city', 'town', 'village', 'capital']
    cou_filter = ['country', 'state', 'province', 'area', 'region']
    org_filter = ['organization', 'company', 'non-profit', 'government', 'union', 'charity', 'agency']

    # Here we loop through the list that contains tags already, but not the best specified yet
    for index, item in enumerate(tagged_list):

        # For every item that has COU as entity we get the definition of the synset using lesk
        if item[1] == 'COU':
            lesk_syn = (lesk(word_list_file, item[0], "n"))
            if lesk_syn is not None:
                lesk_def = lesk_syn.definition()
            else:
                break
            lesk_def_list = lesk_def.split()
            # Then we loop through the definition (which is made into a list) and chech whether words in that list
            # also occur in one of the filters for COU or CIT.
            # If one word does occur in a filter, the loop stops with 'break'.
            for word in lesk_def_list:
                if word in city_filter:
                    item[1] = 'CIT'
                    break
                elif word in cou_filter:
                    item[1] = 'COU'
                    break
                else:
                    item[1] = 'NAT'
        # For every item that has ORG as entity we get the definition of the synset using lesk with the same steps as
        # for COU. But then using the org_filter.
        elif item[1] == 'ORG':
            lesk_syn = (lesk(word_list_file, item[0], "n"))
            if lesk_syn is not None:
                lesk_def = lesk_syn.definition()
            else:
                break
            lesk_def_list = lesk_def.split()
            for word in lesk_def_list:
                if word in org_filter:
                    item[1] = 'ORG'
                    break
                else:
                    item[1] = 'ENT'
    return tagged_list


def write_to_file(word_list_file, wikified_list, output_file, input_file):
    """This function outputs the wikified items to the output files"""
    corrected_list = []

    # Create a list with all words so the index matches the files
    for word in word_list_file:
        corrected_list.append([word])

    # Add the new wikified items to the corrected list
    for item_list in wikified_list:
        if len(item_list) != 3:
            current_index = item_list[2]
            words_in_item = item_list[0].split()
            for word in words_in_item:
                corrected_list[current_index].extend([item_list[1], item_list[3]])
                current_index += 1
            else:
                corrected_list[item_list[2]].extend([item_list[1], item_list[3]])

    # Write the tags and urls to the new files
    with open(output_file, 'w') as output, open(input_file) as input_file:
        for line, item in zip(input_file, corrected_list):
            if len(item) < 3:
                output.write(line)
                continue
            else:
                output.write(line.rstrip() + ' ' + item[1] + ' ' + item[2] + '\n')
            

def wikification (tagged_list, word_list_file):
    """This function wikifies the inputfile, so it filters tags, creates search queries, looks up wikipedialinks
    and writes it to a new en.tok.off.pos.ent1 files"""
    # Create search query strings
    search_query_list = create_search_query_list(tagged_list)

    # Filter the search queries thourgh with lesk to get *possibly* correct tags
    lesk_filtered_list = lesk_filter(search_query_list, word_list_file)

    city_filter = ['city', 'town', 'village', 'capital']
    cou_filter = ['country', 'state', 'province', 'area', 'region']
    org_filter = ['organization', 'company', 'non-profit', 'government', 'union', 'charity', 'agency', 'corporation']

    for item in lesk_filtered_list:
        try:
            page = wikipedia.page(item[0])
            url = page.url
            summary = page.summary
            item.append(url)

        except wikipedia.DisambiguationError as e:
            tag = item[1]
            options = e.options

            # disambiguaty test/filter for ORG/CITY/COU
            if tag == 'ORG' or tag == 'CIT' or tag == 'COU':
                # loop through options (these are possible titles), for every title create a synset with definition

                for title in options:

                    # changing title to query for wordnet.synset search since that does not take spaces but underscores
                    query = title.replace(' ', '_')

                    try:
                        summary = wikipedia.summary(query)
                    except wikipedia.exceptions.DisambiguationError as e:
                        options = e.options[1:]

                    # get the synset that lesk gives for the query
                    # the context used here is the summary of the wikipedia page from the query
                    # this will give a good synset to compare to the one given for the tags
                    lesk_syn = (lesk(summary, query, "n"))
                    if lesk_syn is not None:
                        lesk_def = lesk_syn.definition()
                    else:
                        break
                    lesk_def_list = lesk_def.split()
                    # loop through the words of the synset definition and check if one of them is in one of the filters
                    # if a word is in the filter the loop stops with 'break' and the option_tag is set to that of the
                    # filter
                    for word in lesk_def_list:
                        if word in org_filter:
                            option_tag = 'ORG'
                            break
                        elif word in city_filter:
                            option_tag = 'CIT'
                            break
                        elif word in cou_filter:
                            option_tag = 'COU'
                            break
                    # if the tag that was found in the previous loop is the same as the one that was assigned to the
                    # item it means that it found the matching title to the item based on the entity tag
                    # So the url is that of the title that we were looping at the moment
                    if option_tag == tag:
                        page = wikipedia.page(title)
                        url = page.url
                        item[1] = tag
                        item.append(url)
                        break

            # disambiguaty test/filter for SPO/ENT/ANI
            elif tag == 'ENT' or tag == 'ANI' or tag == 'SPO':
                for title in options:
                    try:
                        page = wikipedia.page(title)
                        summary = wikipedia.summary(page)
                        context_file = summary
                        lesk_syn = lesk(context_file, item[0], 'n')
                    except (wikipedia.exceptions.DisambiguationError, wikipedia.exceptions.PageError):
                        continue

                    if tag == 'ENT':
                        tag_syn = wordnet.synsets("entertainment", pos='n')[0]
                    elif tag == 'ANI':
                        tag_syn = wordnet.synsets("animal", pos='n')[0]
                    elif tag == 'SPO':
                        tag_syn = wordnet.synsets("sport", pos='n')[0]
                    if hypernymOF(lesk_syn, tag_syn):
                        page = wikipedia.page(title)
                        url = page.url
                        item.append(url)

        except wikipedia.exceptions.PageError as e1:
            item = item[:1]
            break
    return lesk_filtered_list
