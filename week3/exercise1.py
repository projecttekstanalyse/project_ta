# exercise1.py
# Authors: Franka Korpel, Wessel Poelman and Iris Meijer
# Date: 7-5-2019

import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet

lemmatizer = WordNetLemmatizer()


def hypernymOF(synset1, synset2):
    '''Checks if the first synset is a hypernym
    of the second synset.'''
    if synset1 == synset2:
        return True
    for hypernym in synset1.hypernyms():
        if synset2 == hypernym:
            return True
        if hypernymOF(hypernym, synset2):
            return True
    return False


def tokenize_sentences(sentences):
    '''tokenizes a list of sentences'''
    tokens = []
    for sent in sentences:
        tokens += nltk.word_tokenize(sent)
    return tokens


def create_noun_list(pos_tag_words):
    '''creates a noun list based on the pos tags
    only includes NN and NNS in this assingment!'''
    noun_list = []
    for item in pos_tag_words:
        if item[1] in {"NN", "NNS"}:
            noun_list.append(item[0])
    return noun_list


def create_synset(noun):
    '''creates synset'''
    return wordnet.synset(noun + '.n.01')


def main():
    # opens file and reads text
    path = "ada_lovelace.txt"
    f = open(path)
    rawText = f.read()
    f.close()

    # create a list of sentences
    sents = nltk.sent_tokenize(rawText)

    # tokenize the words
    tokens = tokenize_sentences(sents)

    # create post tags for the tokens
    post_tags = nltk.pos_tag(tokens)

    # create a list of nouns
    noun_list = create_noun_list(post_tags)

    # lemmatize the nouns from the text
    lemma_list = []
    for noun in noun_list:
        lemma_list.append(lemmatizer.lemmatize(noun, wordnet.NOUN))

    # define the words of interest and store their synsets
    # initialize a counter for the assignment
    relative = [0, wordnet.synsets("relative", pos='n')]
    science = [0, wordnet.synsets("science", pos='n')]
    illness = [0, wordnet.synsets("illness", pos='n')]

    # create a list of tuples where the first item is the lemma
    # and the second a list of synset objects associated with the lemma

    lemma_synset_list = []
    for lemma in lemma_list:
        lemma_synset_list.append((lemma, wordnet.synsets(lemma, pos='n')))

    # check if the synsets match the words of interest
    for syn_tup in lemma_synset_list:
        is_relative = False
        is_science = False
        is_illness = False
        for synset in syn_tup[1]:
            if hypernymOF(synset, relative[1][0]):
                is_relative = True
            if hypernymOF(synset, science[1][0]):
                is_science = True
            if hypernymOF(synset, illness[1][0]):
                is_illness = True
        relative[0] += is_relative
        science[0] += is_science
        illness[0] += is_illness


    print("\n NUMBER OF NOUNS REFERRING TO 'RELATIVE' \n")
    print(relative[0])

    print("\n NUMBER OF NOUNS REFERRING TO 'SCIENCE' \n")
    print(science[0])

    print("\n NUMBER OF NOUNS REFERRING TO 'ILLNESS' \n")
    print(illness[0])

    # Count how many nouns have 1 or more hypernyms
    hypernym_count = {}
    multiple_path_counter =0

    for synset_tup in lemma_synset_list:
        if synset_tup[1]:
            no_hypernyms = len(synset_tup[1][0].hypernyms())
            hypernym_count[no_hypernyms] = hypernym_count.get(no_hypernyms, 0) + 1
        if synset_tup[1] and (len(synset_tup[1][0].hypernym_paths()) > 1):
            multiple_path_counter += 1

    print("\nNUMBER OF NOUNS WITH ONE HYPERNYM")
    print(hypernym_count[1])

    print("\nNUMBER OF NOUNS WITH MULTIPLE HYPERNYM PATHS")
    print(multiple_path_counter)

    total_hypernyms = 0
    for key in hypernym_count:
        total_hypernyms += (key * hypernym_count[key])

    print("\nAVERAGE HYPERNYMS PER NOUN")
    print(total_hypernyms / len(lemma_list))

    # assignment 3
    print("\nSIMILARITY CAR-AUTOMOBILE")
    print(create_synset('car').path_similarity(create_synset('automobile')))
    print("SIMILARITY COAST-SHORE")
    print(create_synset('coast').path_similarity(create_synset('shore')))
    print("SIMILARITY MONK-SLAVE")
    print(create_synset('monk').path_similarity(create_synset('slave')))
    print("SIMILARITY MOON-STRING")
    print(create_synset('moon').path_similarity(create_synset('string')))
    print("SIMILARITY FOOD-FRUIT")
    print(create_synset('food').path_similarity(create_synset('fruit')))
    print("SIMILARITY JOURNEY-CAR")
    print(create_synset('journey').path_similarity(create_synset('car')))


if __name__ == "__main__":
    main()


