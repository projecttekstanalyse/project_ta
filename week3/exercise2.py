# exercise2.py
# Authors: Franka Korpel, Wessel Poelman and Iris Meijer
# Date: 7-5-2019

from nltk.parse import CoreNLPParser
import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet

lemmatizer = WordNetLemmatizer()


def proper_ner_tagger(list):
    ner_tagger = CoreNLPParser(url='http://localhost:9000', tagtype='ner')
    ner = ner_tagger.tag(list)
    proper_noun_ner_list = []
    for word, tag in ner:
        proper_noun_ner_list.append([word,tag])

    return proper_noun_ner_list


def get_ner_dict(list):
    """ Makes a dictionary of how many Persons, Organizations and Locations there are in a text"""
    ner_tagger = CoreNLPParser(url='http://localhost:9000', tagtype='ner')
    ner = ner_tagger.tag(list)
    ner_dict = {}
    for word, name_entity in ner:
        ner_dict[name_entity] = ner_dict.get(name_entity, 0) + 1
    return ner_dict


def create_noun_list(pos_tag_words, option):
    '''creates a noun list based on the pos tags
    only includes NN and NNS in this assingment!'''
    noun_list = []
    for item in pos_tag_words:
        if item[1] in option:
            noun_list.append(item[0])
    return noun_list


def tokenize_sentences(sentences):
    '''tokenizes a list of sentences'''
    tokens = []
    for sent in sentences:
        tokens += nltk.word_tokenize(sent)
    return tokens


def main():
    path = "ada_lovelace.txt"
    f = open(path)
    rawText = f.read()
    f.close()
    print("EXERCISE 1 & 2")
    print(get_ner_dict(rawText.split()))

    # create a list of sentences
    sents = nltk.sent_tokenize(rawText)

    # tokenize the words
    tokens = tokenize_sentences(sents)

    # create post tags for the tokens
    post_tags = nltk.pos_tag(tokens)

    # create a list of nouns
    normal_noun_list = create_noun_list(post_tags, {"NN", "NNS"})
    proper_noun_list = create_noun_list(post_tags, {"NNP"})

    # lemmatize the nouns from the text
    lemma_list = []
    for noun in normal_noun_list:
        lemma_list.append(lemmatizer.lemmatize(noun, wordnet.NOUN))

    lemma_synset_list = []

    for lemma in lemma_list:
        lemma_synset_list.append((lemma, wordnet.synsets(lemma, pos='n')))

    normal_noun_list_classification = []
    for syn_tup in lemma_synset_list:
        if syn_tup[1]:
            normal_noun_list_classification.append([syn_tup[0], syn_tup[1][0]])
    print("\n NORMAL NOUNS CLASSIFICATIONS")
    print(normal_noun_list_classification)
    print("\n PROPER NOUNS CLASSIFICATIONS")
    print(proper_ner_tagger(proper_noun_list))


if __name__ == "__main__":
    main()
