# exercise2.py
# Authors: Franka Korpel, Wessel Poelman and Iris Meijer
# Date: 16-4-2019

import nltk


def longest_sentence(sentence_dictionary):
    """This function returns one random sentence that has the most words"""
    return sorted(sentence_dictionary,
                  key=sentence_dictionary.get)[1]


def shortest_sentence(sentence_dictionary):
    """"This function returns one random sentence that has the least words"""
    return sorted(sentence_dictionary,
                  key=sentence_dictionary.get,
                  reverse=True)[1]


def average_sentence_length(words, sentences):
    """"This function returns the average sentence length"""
    return words/sentences


def ngrams(list, ngram_option=False):
    """"This function returns the top 20 of the most common ngram"""
    if not ngram_option:
        return sorted(nltk.FreqDist(list).items(),
                      reverse=True,
                      key=lambda pair: pair[1])[:20]
    return sorted(nltk.FreqDist(ngram_option(list)).items(),
                  reverse=True,
                  key=lambda pair: pair[1])[:20]


def main():
    # opens file and reads text
    path = "holmes.txt"
    f = open(path)
    rawText = f.read()
    f.close()

    # create a list of sentences
    sents = nltk.sent_tokenize(rawText)

    # initialize the variables for counting
    dict = {}
    all_words = []
    total_words = 0
    total_sentences = 0

    # count the words, sentences and create token list
    for sent in sents:
        words = nltk.word_tokenize(sent)
        all_words += nltk.word_tokenize(sent)
        dict[sent] = len(words)
        total_words += len(words)
        total_sentences += 1

    print("\n LONGEST SENTENCE \n")
    print(longest_sentence(dict))

    print("\n SHORTEST SENTENCE \n")
    print(shortest_sentence(dict))

    print("\n DISTRIBUTION OF SENTENCES IN TERMS OF LENGTH \n")
    freqdist = nltk.FreqDist(dict.values())
    for item in sorted(freqdist.items()):
        print(item)

    print("\n AVARAGE SENTENCE LENGTH \n")
    print(average_sentence_length(total_words, total_sentences))

    char_dict = {}
    word_dict = {}
    all_char = []

    # makes a list and dictionary for all chars
    for char in rawText:
        all_char += char
        char_dict[char] = char_dict.get(char, 0) + 1

    #  makes a list and dictionary for all words
    for word in all_words:
        word_dict[word] = word_dict.get(word, 0) + 1

    print("\n CHARACTER DICTIONARY \n")
    print(char_dict)

    print("\n WORDS DICTIONARY \n")
    print(word_dict)

    # character n-grams
    print("\n CHAR UNIGRAMS \n")
    print(ngrams(all_char))

    print("\n CHAR BIGRAMS \n")
    print(ngrams(all_char, nltk.bigrams))

    print("\n CHAR TRIGRAMS \n")
    print(ngrams(all_char, nltk.trigrams))

    # word n-grams
    print("\n WORD UNIGRAMS \n")
    print(ngrams(all_words))

    print("\n WORD BIGRAMS \n")
    print(ngrams(all_words, nltk.bigrams))

    print("\n WORD TRIGRAMS \n")
    print(ngrams(all_words, nltk.trigrams))


if __name__ == "__main__":
    main()
