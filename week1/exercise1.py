# exercise1.py
# Authors: Franka Korpel, Wessel Poelman and Iris Meijer
# Date: 16-4-2019

from nltk.corpus import gutenberg
import nltk

print("\nThis next print line is slide 15 \n")
# tells which files are in the corpus
print(gutenberg.fileids())

# get the raw text of a corpus
aliceText = gutenberg.raw("carroll-alice.txt")
# print the first 289 characters of the text
print("\nThis next print line is slide 16 \n")
print(aliceText[:289])

# get the words of a corpus as a list
aliceWords = gutenberg.words("carroll-alice.txt")
# print the first 30 words of a corpus as a list
print("\nThis next print line is slide 17 \n")
print(aliceWords[:30])

# get the sentences of a corpus as a list of lists
# (one list of words per sentence)
senseStories = gutenberg.sents("bryant-stories.txt")

# print out the first four sentences
print("\nThis next print line is slide 18 \n")
print(senseStories[:4])

# opens file and reads text
path = "holmes.txt"
f = open(path)
rawText = f.read()
f.close()
print("\nThis next print line is slide 26 \n")
# prints out the first 165 characters of the holmes.txt file
print(rawText[:165])

# splits the text up into sentences
sents = nltk.sent_tokenize(rawText)

# print sentence 21 & 22
print("\nThis next print line is slide 27 \n")
print(sents[20:22])

# Tokenizes the sentences using nltk
tokens = []
for sent in sents:
    tokens += nltk.word_tokenize(sent)
# prints token 301 to 350
print("\nThis next print line is slide 28 \n")
print(tokens[300:350])

sent = ['The', 'cat', 'that', 'sat', 'on', 'the',
        'mat', 'also', 'sat', 'on', 'the', 'sofa']
# turns list into bigrams
bigrams = nltk.bigrams(sent)
# counts frequencies of the bigrams
fdist = nltk.FreqDist(bigrams)
print("\nThis next print line is slide 34 \n")
for b, f in fdist.items():
    # prints every bigram + corresponding frequencies
    print(b, f)
