# File name: WSD.py
# Authors: Wessel Poelman, Iris Meijer, Franka Korpel
# Date: 28-05-2019

import wikipedia
import nltk
from nltk.corpus import wordnet
from nltk.wsd import lesk
import sys


def create_noun_set(pos_tag_words):
    '''creates a noun list based on the pos tags'''
    noun_set = set()
    for item in pos_tag_words:
        if item[1] in {"NN", "NNS"}:
            noun_set.add(item[0].lower())
    return noun_set

def tokenize_sentences(sentences):
    '''tokenizes a list of sentences'''
    tokens = []
    for sent in sentences:
        tokens += nltk.word_tokenize(sent)
    return tokens

def main():

    url_list = ["Bosnia Herzegovina", "United States", "United Nations", "Pakistan",
                "India", "The Hague", "Washington DC", "New Delhi", "Amnesty International"]

    for item in url_list:
        # import wikipedia
        page = wikipedia.page(item)
        page_content = page.content

        # create a list of sentences
        sents = nltk.sent_tokenize(page_content)

        # tokenize the words
        tokens = tokenize_sentences(sents)

        # create post tags for the tokens
        post_tags = nltk.pos_tag(tokens)

        # create a list of nouns
        noun_set = create_noun_set(post_tags)

        polysemous_nouns = []
        total_senses = 0
        sense_count = {}

        for word in noun_set:
            synsets = len(wordnet.synsets(word, "n"))
            if synsets > 1:
                polysemous_nouns.append(word)
                total_senses += synsets
                sense_count[synsets] = sense_count.get(synsets, 0) + 1

        # Lesk print 10th item to check manualy
        lesk_list = []
        for word in polysemous_nouns:
            lesk_list.append(lesk(page_content, word, "n"))

        print("DEFINITION OF 10th SYNSET PER PAGE")
        print(item, lesk_list[10], lesk_list[10].definition())

        print("PROPORTION POYSEMOUS NOUNS PER PAGE")
        proportion = (len(polysemous_nouns))/(len(noun_set))
        print(item, proportion)

        print("AVERAGE NUMBER OF SENSES")
        print(item, (total_senses/len(polysemous_nouns)))

        print("NUMBER OF WORDS WITH AMOUNT OF SYNSETS")
        print(item, sense_count)


if __name__ == "__main__":
    main()





