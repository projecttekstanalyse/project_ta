# exercise2.py
# Authors: Franka Korpel, Wessel Poelman and Iris Meijer
# Date: 30-4-2019

import nltk
from nltk.collocations import *
from nltk.metrics.spearman import *


def rank_tuples(input_list):
    ''' Sorts a list of tuples (item, score) and returns the sorted list'''
    return sorted(input_list, reverse=True, key=lambda pair: pair[1])


def main():
    br_tw = nltk.corpus.brown.tagged_words(categories='mystery')
    br_ts = nltk.corpus.brown.tagged_sents(categories='mystery')

    print("COUNT WORDS")
    print(len(br_tw))

    print("\nCOUNT SENTENCES")
    print(len(br_ts))

    print("\n PRINT WORD 50")
    print(br_tw[49])

    print("\n PRINT WORD 75")
    print(br_tw[74])

    print("\n ALL POST-TAGS")
    word_tags = {}
    words = {}
    adverb_words = {}
    adjective_words = {}
    so_tags = {}
    for word, tag in br_tw:
        if tag in {"RB", "RBR", "RBT", "RN", "RP"}:
            adverb_words[word] = adverb_words.get(word, 0) + 1
        if tag in {"JJ", "JJR", "JJS", "JJT"}:
            adjective_words[word] = adjective_words.get(word, 0) + 1
        if word == "so":
            so_tags[tag] = so_tags.get(tag, 0) + 1
        words[word] = words.get(word, 0) + 1
        word_tags[tag] = word_tags.get(tag, 0) + 1
    print(len(word_tags))

    print("\n TOP 15 WORDS ")
    print(rank_tuples(words.items())[:15])

    print("\n TOP 15 TAGS")
    print(rank_tuples(word_tags.items())[:15])

    sent20dict = {}
    for word, tag in br_ts[19]:
        sent20dict[tag] = sent20dict.get(tag, 0) + 1

    print("\n TAG OF 20TH SENTENCE")
    print(rank_tuples(sent20dict.items()))

    sent40dict = {}
    for word, tag in br_ts[39]:
        sent40dict[tag] = sent20dict.get(tag, 0) + 1

    print("\n TAG OF 40TH SENTENCE")
    print(rank_tuples(sent40dict.items()))

    print("\n MOST FREQUENT ADVERB")
    print(rank_tuples(adverb_words.items())[0])

    print("\n MOST FREQUENT ADJECTIVE")
    print(rank_tuples(adjective_words.items())[0])

    print("\n TAGS OF WORDS 'SO'")
    print(so_tags)

    so_ql_sentences = []
    so_cs_sentences = []
    so_rb_sentences = []
    for sentence in br_ts:
        for word, tag in sentence:
            if word == "so" and tag == "QL":
                so_ql_sentences.append(sentence)
            if word == "so" and tag == "CS":
                so_cs_sentences.append(sentence)
            if word == "so" and tag == "RB":
                so_rb_sentences.append(sentence)
    print("\n PRINT SO  - QL SENTENCE")
    print(so_ql_sentences[0])
    print("\n PRINT SO  - CS SENTENCE")
    print(so_cs_sentences[0])
    print("\n PRINT SO  - RB SENTENCE")
    print(so_rb_sentences[0])

    so_tags_dict = {
        "QL": {
            "pre": {},
            "post": {}
        },
        "CS": {
            "pre": {},
            "post": {},
        },
        "RB": {
            "pre": {},
            "post": {}
        }
    }

    for sentence in br_ts:
        for word, tag in sentence:
            if word == "so":
                so_index = sentence.index((word, tag))
                pre_context = sentence[so_index - 1]
                post_context = sentence[so_index + 1]
                if tag == "QL":
                    so_tags_dict["QL"]["pre"][pre_context[1]] = so_tags_dict["QL"]["pre"].get(pre_context[1], 0) + 1
                    so_tags_dict["QL"]["post"][post_context[1]] = so_tags_dict["QL"]["post"].get(post_context[1], 0) + 1
                if tag == "CS":
                    so_tags_dict["CS"]["pre"][pre_context[1]] = so_tags_dict["CS"]["pre"].get(pre_context[1], 0) + 1
                    so_tags_dict["CS"]["post"][post_context[1]] = so_tags_dict["CS"]["post"].get(post_context[1], 0) + 1
                if tag == "RB":
                    so_tags_dict["RB"]["pre"][pre_context[1]] = so_tags_dict["RB"]["pre"].get(pre_context[1], 0) + 1
                    so_tags_dict["RB"]["post"][post_context[1]] = so_tags_dict["RB"]["post"].get(post_context[1], 0) + 1

    print("\n SO TAGS DICTIONARY \n")

    print("\n MOST FREQUENT TAG PRE QL SO \n")
    print(rank_tuples(so_tags_dict["QL"]["pre"].items())[0])

    print("\n MOST FREQUENT TAG POST QL SO \n")
    print(rank_tuples(so_tags_dict["QL"]["post"].items())[0])

    print("\n MOST FREQUENT TAG PRE CS SO \n")
    print(rank_tuples(so_tags_dict["CS"]["pre"].items())[0])

    print("\n MOST FREQUENT TAG POST CS SO \n")
    print(rank_tuples(so_tags_dict["CS"]["post"].items())[0])

    print("\n MOST FREQUENT TAG PRE RB SO \n")
    print(rank_tuples(so_tags_dict["RB"]["pre"].items())[0])

    print("\n MOST FREQUENT TAG POST RB SO \n")
    print(rank_tuples(so_tags_dict["RB"]["post"].items())[0])

    # HET PROGRAMMA WORDT HEEL SLOOM VAN DEZE CODE, DUS ALS JE WAT WIL TESTEN DIT EVEN COMMENTEN
    print("\n POS-TAG HOLMES.TXT")
    text = open('holmes.txt').read()
    tokens = nltk.word_tokenize(text)
    holmes_pos = nltk.pos_tag(tokens)

    print("\n TOP 5 POS COLLOCATIONS CHI \n")
    only_pos_holmes = []
    for tuple in holmes_pos:
        pos = tuple[1]
        only_pos_holmes.append(pos)

    bigram_measures = nltk.collocations.BigramAssocMeasures()
    finder = BigramCollocationFinder.from_words(only_pos_holmes)
    col_list_chi = finder.nbest(bigram_measures.chi_sq, 5)

    # prints the collocations 1 by 1 including the information about the pos-tags
    for item in col_list_chi:
        print('Collocation: {0}'.format(item))
        nltk.help.upenn_tagset(item[0])
        nltk.help.upenn_tagset(item[1])

    print("\n TOP 5 POS COLLOCATIONS RAW FREQ \n")
    col_list_raw = finder.nbest(bigram_measures.raw_freq, 5)

    # prints the collocations 1 by 1 including the information about the pos-tags
    for item in col_list_raw:
        print('Collocation: {0}'.format(item))
        nltk.help.upenn_tagset(item[0])
        nltk.help.upenn_tagset(item[1])


if __name__ == "__main__":
    main()