# exercise1.py
# Authors: Franka Korpel, Wessel Poelman and Iris Meijer
# Date: 30-4-2019

import nltk
from nltk.collocations import *
from nltk.metrics.spearman import *


def pmi(finder, bigram_measures):
    """" This function returns the most likely 20 collocations using PMI"""
    return finder.nbest(bigram_measures.pmi, 20)


def chi_square(finder, bigram_measures):
    """ This function returns most likely 20 collocations using chi_square"""
    return finder.nbest(bigram_measures.chi_sq, 20)


def spearman(list1, list2):
    return ('%0.1f' % spearman_correlation(list1, list2))


def main():
    text = open('holmes.txt').read()
    tokens = nltk.wordpunct_tokenize(text)
    bigram_measures = nltk.collocations.BigramAssocMeasures()
    finder = BigramCollocationFinder.from_words(tokens)

    # Prints PMI
    print("\n PMI \n")
    print(pmi(finder, bigram_measures))

    # Prints Chi-square
    print("\n CHI-SQUARE \n")
    print(chi_square(finder, bigram_measures))

    # Prints Spearman Correlation
    print("\n SPEARMAN \n")
    pmi_result = pmi(finder, bigram_measures)
    chi_result = chi_square(finder, bigram_measures)
    print(spearman_correlation(
        ranks_from_sequence(pmi_result),
        ranks_from_sequence(chi_result)))


if __name__ == "__main__":
    main()
